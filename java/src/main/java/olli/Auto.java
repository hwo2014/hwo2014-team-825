package olli;


public class Auto
{	
	String name;
  String color;
  double length;
  double width;
  double flag;
    
  double nopeus = 0;
  double heilahdus = 0;
  double heilahdusKiihtyvyys = 0;
  String heilahdusSuunta;
  
  double lukkojarruja = 0;
  double tallapohjassa = 0;
  
  double etaisyysMunautosta = Integer.MAX_VALUE;
  
  int edellinenTick;
  double edellinenThrottle;
  double edellinenHeilahdus;
  double edellinenAngle;  
  Ratapala edellinenPala;
  double edellinenPaikka;
    
  int tick = 0;
  double angle;
  Ratapala pala;
  double paikka;
  double jaljella;
  int lap = 0;
  
  boolean vaihtaaKaistaa = false;
  
  String manooveri = AI_interface.START;
  
  int laneIndex;
  
  public void paivita(int tick, Double angle, int lap, int pieceIndex, double inPieceDistance, int startLaneIndex, int endLaneIndex)
  {
  	//if (pala.length<inPieceDistance) pala.length = inPieceDistance; //ulkorata pitempi kuin pala...
  	  	
  	edellinenTick = this.tick;
  	this.tick = tick;
  	
  	laneIndex = endLaneIndex;
  	
  	edellinenAngle = this.angle;
  	this.angle = angle;
  	this.lap = lap;
  	edellinenPaikka = paikka;
  	paikka = inPieceDistance;
  	
  	double kuljettuMatka;
  	if (pieceIndex!=pala.index)
  	{
  		edellinenPala = pala;
  		pala = pala.seuraava;
  		
  		kuljettuMatka = paikka + edellinenPala.length[startLaneIndex] - edellinenPaikka;
  		if (pala.switchi) manooveri = AI_interface.NULL;
  	  	  
  	  lukkojarruja = 0;
  	  tallapohjassa = 0;
  	}
  	else kuljettuMatka = paikka - edellinenPaikka;
  	
  	nopeus = kuljettuMatka / (tick - edellinenTick);
  	
  	edellinenHeilahdus = heilahdus;
  	
  	heilahdus = angle - edellinenAngle;
  	if (heilahdus<0) heilahdus = -heilahdus;
  	if (heilahdus == 0) heilahdusSuunta = AI_interface.STRAIGHT;
  	else if (angle < edellinenAngle) heilahdusSuunta = AI_interface.LEFT;
  	else heilahdusSuunta = AI_interface.RIGHT; 
  	
  	heilahdusKiihtyvyys = heilahdus - edellinenHeilahdus;
    	  	
  	jaljella = pala.length[endLaneIndex] - paikka;
  	
  	vaihtaaKaistaa = startLaneIndex != endLaneIndex;
  }
}
