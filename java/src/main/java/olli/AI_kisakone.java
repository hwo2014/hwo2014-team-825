package olli;

import java.io.BufferedReader;
import java.util.Arrays;

import noobbot.Main;

public class AI_kisakone extends AI_kori
{			
	public AI_kisakone(Main main, BufferedReader reader)
	{
		super(main, reader);
	}
	
	public void crash(Auto auto)
	{				
		if (auto!=munauto) return;
		
		Ratapala crashi = auto.pala;

		{
			System.out.println(auto.name+"----------------------------------CRASH tapahtuma palassa "+crashi.index);

			//if (crashi.tyyppi==STRAIGHT) crashi=crashi.edellinen;
			
			while (crashi.tyyppi==STRAIGHT) crashi=crashi.edellinen;
			
		  omatCrashiKohdat.add(crashi);
		  
		  Ratapala eka = crashi;
		
	  	for (int l=0; l<ajoLinjoja; l++)
	  	{
	  		int i = 0;
	  		crashi = eka;
	  		while (i<5)
	  		{
	  			double uusi;
	  			if (munauto.nopeus<crashi.oletusNopeus[l]) uusi=munauto.nopeus*0.85;
	  			else uusi = crashi.oletusNopeus[l]*0.75;
	  			//System.out.println(crashi.index+": "+l+": "+crashi.oletusNopeus[l]+"->"+uusi);
	  			crashi.oletusNopeus[l]=uusi;
	  			crashi=crashi.edellinen;
	  			i++;
	  		}
	  	/*	if (munauto.nopeus<crashi.oletusNopeus[l]) crashi.oletusNopeus[l]=munauto.nopeus*0.95;
	  		else crashi.oletusNopeus[l]*=0.9;
	  		crashi.edellinen.oletusNopeus[l]*=0.9;
	  		crashi.edellinen.edellinen.oletusNopeus[l]*=0.8;
	  		crashi.edellinen.edellinen.edellinen.oletusNopeus[l]*=0.9;
	  		crashi.edellinen.edellinen.edellinen.edellinen.oletusNopeus[l]*=0.9;*/
		  }
	  	
	  /*	if (munauto.lap==1)
	  	{
				for (int i = 0; i<rata.length; i++)
				{
					if (rata[i].maxKulma==0 || rata[i].maxKulma>50)
					for (int l=0; l<ajoLinjoja; l++)
				  {					  
						rata[i].oletusNopeus[l]*=0.9;
				  }
				}
	  	}*/
		}
	}
	
	class Uhka implements Comparable<Uhka>
	{
		int kaista;
		double etaisyysMunautosta = Integer.MAX_VALUE;
		double nopeus = Integer.MAX_VALUE;
	
		@Override
		public int compareTo(Uhka o)
		{
			if (this.kaista==munauto.pala.seuraava.seuraava.valittuAjoLinja || o.kaista==munauto.pala.seuraava.seuraava.valittuAjoLinja)
			{
				//+1=o, -1=this
				
				Uhka oletusLinja = this;
				Uhka pidempiLinja = o;
				if (o.kaista==munauto.pala.seuraava.seuraava.valittuAjoLinja)
				{
					oletusLinja = o;
					pidempiLinja = this;
				}
								
				if (munauto.pala.seuraava.seuraava.tyyppi!=STRAIGHT && munauto.nopeus/munauto.pala.oletusNopeus[pidempiLinja.kaista]>1.1)
				{
					double oletusJyrkkyys = Ratapala.kokonaisJyrkkyys(munauto.pala.seuraava.seuraava, oletusLinja.kaista);
					double pidempiJyrkkyys = Ratapala.kokonaisJyrkkyys(munauto.pala.seuraava.seuraava, pidempiLinja.kaista);
					if (pidempiJyrkkyys>oletusJyrkkyys)
					{
						return oletusLinja == this ? -1 : 1;
					}
				}
				
				if (this.etaisyysMunautosta==Integer.MAX_VALUE) return -1;
				if (o.etaisyysMunautosta==Integer.MAX_VALUE) return 1;
				
				if (Math.pow(oletusLinja.etaisyysMunautosta, 2)*Math.pow(oletusLinja.nopeus, 1.5) >= Math.pow(pidempiLinja.etaisyysMunautosta, 2)*Math.pow(pidempiLinja.nopeus, 1.5))
				{
					return oletusLinja == this ? -1 : 1;
				}
				
				double oletusMatka = Ratapala.etaisyys(munauto.pala.seuraava.seuraava, munauto.pala.seuraava.seuraavaVaihde, oletusLinja.kaista);
				double pidempiMatka = Ratapala.etaisyys(munauto.pala.seuraava.seuraava, munauto.pala.seuraava.seuraavaVaihde, pidempiLinja.kaista);
				
				if (oletusLinja.etaisyysMunautosta<40 && oletusLinja.nopeus/munauto.nopeus<1 && (pidempiMatka/oletusMatka<1.2 || oletusLinja.nopeus/munauto.nopeus<0.8))
				{
					return pidempiLinja == this ? -1 : 1;
				}
				
				return oletusLinja == this ? -1 : 1;
			}
			
			double oletusJyrkkyys = Ratapala.kokonaisJyrkkyys(munauto.pala.seuraava.seuraava, munauto.pala.seuraava.seuraava.valittuAjoLinja);
			
			if (munauto.pala.seuraava.seuraava.tyyppi!=STRAIGHT)
			{
				if (munauto.nopeus/munauto.pala.oletusNopeus[this.kaista]>1.1)
			  {
          double jyrkkyys = Ratapala.kokonaisJyrkkyys(munauto.pala.seuraava.seuraava, this.kaista);
          if (jyrkkyys>oletusJyrkkyys)
          {
          	return 1;
          }
			  }
				
				if (munauto.nopeus/munauto.pala.oletusNopeus[o.kaista]>1.1)
			  {
          double jyrkkyys = Ratapala.kokonaisJyrkkyys(munauto.pala.seuraava.seuraava, o.kaista);
          if (jyrkkyys>oletusJyrkkyys)
          {
          	return -1;
          }
			  }
			}
			
			if (this.etaisyysMunautosta==Integer.MAX_VALUE) return -1;
			if (o.etaisyysMunautosta==Integer.MAX_VALUE) return 1;
			
			if (Math.pow(this.etaisyysMunautosta, 2)*Math.pow(this.nopeus, 1.5) >= Math.pow(o.etaisyysMunautosta, 2)*Math.pow(o.nopeus, 1.5)) return -1;
			
			return 1;
		}
	}
			
	public String vaihdaKaistaa()
	{
		if (vaihteetonKisa) return STRAIGHT;
		String vanhaPaatos = super.vaihdaKaistaa();
		if (!munauto.pala.seuraava.switchi) return vanhaPaatos;
		if (!uhkaEdessa()) return vanhaPaatos;
		
		Uhka[] uhat = uhat();		
		String uusiPaatos = ohjausLiike(uhat);
		
		if (uusiPaatos==TURBO) return TURBO;
		if (uusiPaatos==munauto.manooveri) return munauto.manooveri;
		munauto.manooveri = uusiPaatos;
		if (vanhaPaatos==munauto.manooveri) return munauto.manooveri;
		laskeValittuAjolinja(munauto.laneIndex, munauto.pala.seuraavaVaihde, munauto.manooveri);
		System.out.println(vanhaPaatos+" -> kiertoliike: "+uusiPaatos);
    return munauto.manooveri;
	}
	
	public boolean uhkaEdessa()
	{
		for (Auto auto: autot.values())
		{
			if (auto==munauto) continue;
			if (auto.laneIndex!=munauto.pala.seuraava.seuraava.valittuAjoLinja) continue;
			auto.etaisyysMunautosta = Ratapala.etaisyys(munauto.pala, auto.pala);
			if (auto.etaisyysMunautosta == 0 && auto.paikka<munauto.paikka) continue;
			if (auto.etaisyysMunautosta>Ratapala.etaisyys(munauto.pala, munauto.pala.seuraavaVaihde.seuraavaVaihde)) continue;
      return true;
		}
		return false;
	}
	
	public Uhka[] uhat()
	{
		Uhka[] uhat = new Uhka[ajoLinjoja];
		for (int l=0; l<ajoLinjoja; l++)
		{
			Uhka u = new Uhka();
			u.kaista = l;
			uhat[l] = u;
			for (Auto auto: autot.values())
			{
				if (auto==munauto) continue;
				if (auto.laneIndex!=l) continue;
				auto.etaisyysMunautosta = Ratapala.etaisyys(munauto.pala, auto.pala);
				if (auto.etaisyysMunautosta>Ratapala.etaisyys(munauto.pala, munauto.pala.seuraavaVaihde.seuraavaVaihde)) continue;
        if (auto.etaisyysMunautosta + auto.paikka<u.etaisyysMunautosta) u.etaisyysMunautosta = auto.etaisyysMunautosta + auto.paikka;
			}
		}
		Arrays.sort(uhat);
		return uhat;
	}
	
	public String ohjausLiike(Uhka[] uhat)
	{
		for (int l=0; l<uhat.length; l++)
		{
			if (uhat[l].kaista<munauto.laneIndex-1 || uhat[l].kaista>munauto.laneIndex+1) continue;
			if (uhat[l].kaista == munauto.laneIndex) return STRAIGHT;
			if (uhat[l].kaista < munauto.laneIndex) return LEFT;
			return RIGHT;
		}
		
		System.out.println("Ohjausliikeen laskenta epaonnistui");
		return munauto.manooveri;
	}
	
	public Auto autoEtaisyydet()
	{
		Auto lahinAuto = null;
		
		for (Auto auto: autot.values())
		{
			auto.etaisyysMunautosta = Integer.MAX_VALUE;
			if (auto==munauto) continue;
			if (auto.laneIndex!=auto.pala.valittuAjoLinja) continue;
			auto.etaisyysMunautosta = Ratapala.etaisyys(munauto.pala, auto.pala);
			if (auto.etaisyysMunautosta>Ratapala.etaisyys(munauto.pala, munauto.pala.seuraavaVaihde.seuraavaVaihde)) continue;
			if (lahinAuto==null || auto.etaisyysMunautosta<lahinAuto.etaisyysMunautosta) lahinAuto = auto;
		}
		
		if (lahinAuto!=null && lahinAuto.nopeus < munauto.nopeus) return lahinAuto;
		
		return null;

	}
}
