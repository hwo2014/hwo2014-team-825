package olli;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import noobbot.Main;


public abstract class AI_interface
{
  public static final String LEFT = "Left";
  public static final String RIGHT = "Right";
  public static final String STRAIGHT = "Straight";
  public static final String NULL = "Null";
  public static final String START = "Start";
  public static final String TURBO = "Turbo";
	
	public boolean qualifiedRace = false;
  public final Main main;
  public Ratapala[] rata;  
  public HashMap<String, Auto> autot = new HashMap<>();
  
  public boolean vaihteetonKisa = false;
  public int ajoLinjoja;
  public boolean turboKaytossa = false;
  public int vikaKierros = 1000;
  
  Auto munauto;
  
  static final JsonParser parser = new JsonParser();
  
  final BufferedReader reader;
  
  public abstract void alustaVauhdinjako();
  public abstract void aja();
  public abstract void palaVaihtuu();
  public abstract void crash(Auto auto);
  public abstract void kierrosVaihtui(Auto auto);
  public abstract void turbo(int turboDurationMilliseconds, int turboDurationTicks, double turboFactor);
  
  public AI_interface(Main main, final BufferedReader reader)
  {
    this.main = main;
  	this.reader = reader;
  }
  
  public void drive() throws JsonSyntaxException, IOException
  {
  	String line = null;
  	String munvari = null;
  	
  	while(true)
    {        	
    	line = reader.readLine();
    	if (line==null)
    	{
    		System.out.println("kaatui ennen starttia");
    		return;
    	}    	
    	final JsonObject  msg = parser.parse(line).getAsJsonObject();
    	final String msgType = msg.getAsJsonPrimitive("msgType").getAsString();
    	
      if (msgType.equals("join"))
      {
        System.out.println("join ok");
      }
      else if (msgType.equals("yourCar"))
      {
        munvari = msg.getAsJsonObject("data").get("color").getAsString();
        System.out.println("yourCar = "+munvari);
      }
      else if (msgType.equals("gameInit"))
      {
        initRace(msg, munvari);
        System.out.println("gameInit ok: "+munvari);
      }
      else if (msgType.equals("carPositions"))
      {
      	viivalle(msg);
        System.out.println("carPositions ok");
      }
      else if (msgType.equals("gameStart"))
      {
      	main.send(main.new Throttle(1));
        System.out.println("gameStart ok");
        break;
      }
      else System.out.println(line);
    }
  		
    while((line = reader.readLine()) !=null)
    {
    	final JsonObject msg = parser.parse(line).getAsJsonObject();
    	final String msgType = msg.getAsJsonPrimitive("msgType").getAsString();
    	
      if (msgType.equals("carPositions"))
      { 
      	paivita(msg);
      	aja();
      }
      else if (msgType.equals("turboAvailable"))
      {
      	turboMsg(msg);
      }
      else if (msgType.equals("crash"))
      { 
      	crash(autot.get(msg.getAsJsonObject("data").get("color").getAsString()));
      }
      else if (msgType.equals("lapFinished"))
      {
      	kierrosVaihtui(autot.get(msg.getAsJsonObject("data").getAsJsonObject("car").get("color").getAsString()));
      }
      else if (msgType.equals("finish"))
      {
      	if (munauto == autot.get(msg.getAsJsonObject("data").get("color").getAsString()))
      	{
      		System.out.println(munauto.color+": finish ok");
      		//break;
      	}
      }
      else if (msgType.equals("gameEnd"))
      {
        System.out.println("gameEnd ok");
        if (!qualifiedRace) break;
      }
      else if (msgType.equals("tournamentEnd"))
      { 
      	System.out.println("tournamentEnd ok");
      	break;
      }
      else
      {
      	main.send(main.new Ping());
      	System.out.println(msgType);
      }
    }
  }
    
  public void initRace(JsonObject msg, String munvari)
  {
  	JsonObject data = msg.getAsJsonObject("data");
  	JsonObject race = data.getAsJsonObject("race");
  	JsonObject raceSession = race.getAsJsonObject("raceSession");
  	boolean qualification = raceSession.has("durationMs");  	
    JsonObject track = race.getAsJsonObject("track");
    JsonArray pieces =  track.getAsJsonArray("pieces");
    JsonArray lanes =  track.getAsJsonArray("lanes");
    JsonArray cars = race.getAsJsonArray("cars");
    if (qualification || !qualifiedRace) laskeAjolinjat(lanes, luoRata(pieces, lanes));
    luoAutot(cars, munvari);
    
    //for (Auto auto: autot.values()) auto.pala = ekapala;
    if (qualification || !qualifiedRace) alustaVauhdinjako();
    
    if (qualification) vikaKierros = 100000;
    else vikaKierros = raceSession.getAsJsonPrimitive("laps").getAsInt()-1; 
    
    if (qualification) qualifiedRace = true;
  }
  
  public int luoRata(JsonArray pieces, JsonArray lanes)
  {
  	ajoLinjoja = lanes.size();
  	
  	int[] laneDistances = new int[lanes.size()];
  	for (int i=0; i<laneDistances.length; i++)
  	{
  		laneDistances[i] = ((JsonObject) lanes.get(i)).get("distanceFromCenter").getAsInt();
  	}
  	
  	rata = new Ratapala[pieces.size()];
  	
  	Ratapala seuraavaVaihde = null;
  	
  	for (int i = 0; i<rata.length; i++)
  	{
  		JsonObject piece = (JsonObject) pieces.get(i);
  		rata[i] = new Ratapala();
  		rata[i].index = i;
  		rata[i].radius = new double[laneDistances.length];
  		rata[i].length = new double[laneDistances.length];
  		
  		if (piece.has("radius"))
  		{  			
  			int angle = piece.get("angle").getAsInt();
  			if (angle<0)
  			{
  				rata[i].tyyppi = LEFT;
  				angle = -angle;
  			}
  			else rata[i].tyyppi = RIGHT;
  			
  			for (int l=0; l<laneDistances.length; l++)
  	  	{
  				if (rata[i].tyyppi == LEFT)
  				  rata[i].radius[l] = piece.get("radius").getAsInt()+laneDistances[l];
  				else rata[i].radius[l] = piece.get("radius").getAsInt()-laneDistances[l];
  	  	}
  			
   			
  			for (int l=0; l<laneDistances.length; l++)
  	  	{
  				rata[i].length[l] = (int)(Math.PI * 2 * rata[i].radius[l] * angle)/360;
  	  	}
  			
  		}
  		else
  		{
  			rata[i].tyyppi=STRAIGHT;
  			
  			for (int l=0; l<laneDistances.length; l++)
  	  	{
  				rata[i].radius[l] = Integer.MAX_VALUE;
    			rata[i].length[l] = piece.get("length").getAsInt();
  	  	}
  		}
  		
  		if (piece.has("switch"))
  		{
  			rata[i].switchi = true;
  			
  			if (seuraavaVaihde==null) seuraavaVaihde = rata[i];
  		}
  		
  		if (i>0) rata[i].edellinen = rata[i-1];
  	}
  	
  	rata[0].edellinen =rata[rata.length-1];
  	rata[rata.length-1].seuraava = rata[0];
  		  	
  	if (seuraavaVaihde == null)
  	{
  		vaihteetonKisa = true;
  		return 0;
  	}
  	
  	int vaihteita = 1;
  	  	
  	rata[rata.length-1].seuraavaVaihde = seuraavaVaihde;
		  	
   	if (rata[rata.length-1].switchi)
   	{
   		vaihteita++;
   		seuraavaVaihde = rata[rata.length-1];
   	}
  	 
  	for (int i = rata.length-2; i>-1; i--)
  	{
  		rata[i].seuraava = rata[i+1];
  		rata[i].seuraavaVaihde = seuraavaVaihde;
  		if (rata[i].switchi)
  		{
  			vaihteita++;
  			seuraavaVaihde = rata[i];
  		}
  	}
  	
  	loppuSuora();
  	
  	return vaihteita;
  }
    
  public void loppuSuora()
  {
  	rata[rata.length-1].loppuSuora = true;
  	rata[rata.length-2].loppuSuora = true;
  	
  	for (int i = rata.length-3; i>0; i--)
  	{
  		if (rata[i].tyyppi==STRAIGHT) rata[i].loppuSuora = true; else break;
  	}
  }
  
  public void laskeAjolinjat(JsonArray lanes, int vaihteita)
  {
  	if (vaihteetonKisa) return;

    Ratapala eka = rata[0].seuraavaVaihde;
    Ratapala nyt = eka.seuraavaVaihde;
    
    while (true)
    {
    	nyt.oletusManooveri = new String[lanes.size()];

    	for (int linja = 0; linja<ajoLinjoja; linja++)
    	{
    		double left = Integer.MAX_VALUE;
      	double straight = Integer.MAX_VALUE;
      	double right = Integer.MAX_VALUE;
      	
      	if (linja > 0) left = laskePituus(nyt.seuraavaVaihde, linja-1, Ratapala.etaisyys(nyt, nyt.seuraavaVaihde, linja-1), 0);
      	straight = laskePituus(nyt.seuraavaVaihde, linja, Ratapala.etaisyys(nyt, nyt.seuraavaVaihde, linja), 0);
      	if (linja < ajoLinjoja-1) right = laskePituus(nyt.seuraavaVaihde, linja+1, Ratapala.etaisyys(nyt, nyt.seuraavaVaihde, linja+1), 0);
        
      	if (left < straight)
      	{
      		if (right < left) nyt.oletusManooveri[linja] = RIGHT;
      		else nyt.oletusManooveri[linja] = LEFT;
      	}
      	else if (right < straight) nyt.oletusManooveri[linja] = RIGHT;
      	else nyt.oletusManooveri[linja] = STRAIGHT;
    	}
    	
    	if (nyt==eka) break;
    	nyt = nyt.seuraavaVaihde;
    }
  }
  
  public double laskePituus(Ratapala nyt, int linja, double nytPituus, int kohta)
  {
  	if (kohta == 5) return nytPituus;
  	
  	double left = Integer.MAX_VALUE;
  	double straight = Integer.MAX_VALUE;
  	double right = Integer.MAX_VALUE;
  	
  	if (linja > 0) left = nytPituus+laskePituus(nyt.seuraavaVaihde, linja-1, Ratapala.etaisyys(nyt, nyt.seuraavaVaihde, linja-1), kohta+1);
  	straight = nytPituus+laskePituus(nyt.seuraavaVaihde, linja, Ratapala.etaisyys(nyt, nyt.seuraavaVaihde, linja), kohta+1);
  	if (linja < ajoLinjoja-1) right = nytPituus+laskePituus(nyt.seuraavaVaihde, linja+1, Ratapala.etaisyys(nyt, nyt.seuraavaVaihde, linja+1), kohta+1);
    
  	if (left < straight)
  	{
  		if (right < left) return right;
  		return left;
  	}
  	else if (right < straight) return right;
  	return straight;  	
  }	
  
  public void luoAutot(JsonArray cars, String munvari)
  {
  	autot.clear();
  	  	
  	for (int i = 0; i<cars.size(); i++)
  	{
  		JsonObject car = (JsonObject) cars.get(i);
  		JsonObject id = car.getAsJsonObject("id");
  		Auto auto = new Auto();
  		auto.name = id.get("name").getAsString();
  		auto.color = id.get("color").getAsString();
  		
  		JsonObject dimensions = car.getAsJsonObject("dimensions");
  		auto.length = dimensions.get("length").getAsInt();
  		auto.width = dimensions.get("width").getAsInt();
  		auto.flag = dimensions.get("guideFlagPosition").getAsInt();
  		
  		if (munvari.equals(auto.color)) munauto = auto;
  		autot.put(auto.color, auto);
  	}
  }
  
  public void viivalle(JsonObject msg)
  {
  	JsonArray data = msg.getAsJsonArray("data");
  	
  	for (int i = 0; i<data.size(); i++)
  	{
  		JsonObject car = (JsonObject) data.get(i);
  		JsonObject id = car.getAsJsonObject("id");
  		Auto auto = autot.get(id.get("color").getAsString());
  		auto.angle = car.get("angle").getAsDouble();
  		JsonObject position =  car.getAsJsonObject("piecePosition");
  		auto.pala = rata[position.get("pieceIndex").getAsInt()];
  		auto.paikka = position.get("inPieceDistance").getAsInt();
  		JsonObject lane = position.getAsJsonObject("lane");
  		auto.laneIndex = lane.get("startLaneIndex").getAsInt();
  	}
  }
  
  public void paivita(JsonObject msg)
  {
    JsonArray data = msg.getAsJsonArray("data");
    
    if (!msg.has("gameTick"))
    {
    	//Main.o("kummallinen carpositions: ", msg.toString());
    	return;
    }
    
  	int tick = msg.getAsJsonPrimitive("gameTick").getAsInt();
  	  	
  	for (int i = 0; i<data.size(); i++)
  	{
  		JsonObject car = (JsonObject) data.get(i);
  		JsonObject id = car.getAsJsonObject("id");
  		Auto auto = autot.get(id.get("color").getAsString());
  		JsonObject position =  car.getAsJsonObject("piecePosition");
  		JsonObject lane = position.getAsJsonObject("lane");
  		
  		int pala = position.get("pieceIndex").getAsInt();
  		
  		if (auto==munauto && pala!=auto.pala.index) palaVaihtuu();
  		  		
  		auto.paivita(
  			tick,
  			car.get("angle").getAsDouble(),
  			position.get("lap").getAsInt(),
  			pala,
  			position.get("inPieceDistance").getAsDouble(),
  			lane.get("startLaneIndex").getAsInt(),
  			lane.get("endLaneIndex").getAsInt()
  	  );
  	}
  }
  
  public void turboMsg(JsonObject msg)
  {
    JsonObject data = msg.getAsJsonObject("data");
  	int turboDurationMilliseconds = data.getAsJsonPrimitive("turboDurationMilliseconds").getAsInt();
  	int turboDurationTicks = data.getAsJsonPrimitive("turboDurationTicks").getAsInt();
  	double turboFactor = data.getAsJsonPrimitive("turboFactor").getAsDouble();
  	turbo(turboDurationMilliseconds, turboDurationTicks, turboFactor);
  }
}
