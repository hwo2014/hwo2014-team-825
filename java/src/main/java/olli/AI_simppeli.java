package olli;

import java.io.BufferedReader;

import noobbot.Main;
import noobbot.Main.SwitchLane;
import noobbot.Main.Throttle;

public class AI_simppeli extends AI_interface
{
  double throttle;
	
	public AI_simppeli(Main main, BufferedReader reader)
	{
		super(main, reader);
		throttle = 0.1 + Math.random()/4;
	}

	@Override
	public void alustaVauhdinjako()
	{

	}

	@Override
	public void aja()
	{
		if (Math.random()<0.1) main.send(main.new SwitchLane(false));
		else if (Math.random()<0.9) main.send(main.new SwitchLane(true));
		else main.send(main.new Throttle(throttle));
	}

	@Override
	public void palaVaihtuu()
	{

	}

	@Override
	public void crash(Auto auto)
	{
	
	}

	@Override
	public void kierrosVaihtui(Auto auto)
	{

	}

	@Override
	public void turbo(int turboDurationMilliseconds, int turboDurationTicks,
			double turboFactor)
	{

	}

}
