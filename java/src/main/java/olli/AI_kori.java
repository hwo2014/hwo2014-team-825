package olli;

import java.io.BufferedReader;
import java.util.ArrayList;

import noobbot.Main;


public abstract class AI_kori extends AI_interface
{
	final double crashiKulma = 60;
	double rauhallisuus = 1100;
	
	ArrayList<Ratapala> omatCrashiKohdat = new ArrayList<>();

	double edThrottle = 0;
	String valittuManooveri = NULL;
	
	public AI_kori(Main main, BufferedReader reader)
	{
		super(main, reader);
	}

	public void aja()
	{
		String manooveri = vaihdaKaistaa();
		if (manooveri == STRAIGHT || manooveri == valittuManooveri) kaasuta();
		else if (manooveri == TURBO)
		{
			main.send(main.new Turbo());
  	  turboKaytossa = false;
		}
		else	
		{
			System.out.println(manooveri);
			if (manooveri == LEFT) main.send(main.new SwitchLane(false));
			else main.send(main.new SwitchLane(true));
			valittuManooveri = manooveri;
		}
	}
	
	boolean jarrutpohjassa = false;
	boolean mutkassa = false;
	
	public void kaasuta()
	{						
		double throttle = ennakoiKaasu();
		double maxThrottle = maxKaasu();
		
		if (maxThrottle<throttle) throttle = maxThrottle;
		
		main.send(main.new Throttle(throttle));
		
		edThrottle = throttle;		
		
		double angle = munauto.angle;
		if (angle<0) angle = -angle;

		if (angle>55) System.out.println(munauto.pala.index+": "+angle);
		
		if (angle>munauto.pala.maxKulma) munauto.pala.maxKulma = angle;
		if (munauto.nopeus>munauto.pala.maxNopeus) munauto.pala.maxNopeus = munauto.nopeus;
	}
	
	public void turbo(int turboDurationMilliseconds, int turboDurationTicks, double turboFactor)
	{
  	turboKaytossa = true;
	}
	
	public double ennakoiKaasu()
	{		
		Ratapala kohde = munauto.pala.seuraava;
		if (munauto.lap == vikaKierros && munauto.pala.loppuSuora  || kohde.loppuSuora) return 1;
		
		for (int i=0; i<7; i++)
		{
			double etaisyysKohteeseen = Ratapala.etaisyys(munauto.pala, kohde) - munauto.paikka;
			double nopeusKohteessa = kohde.oletusNopeus[kohde.valittuAjoLinja];
			
			if (!ehtiiKaasuttaa(etaisyysKohteeseen, nopeusKohteessa))
			{
				return 0;
			}
			kohde = kohde.seuraava;
		}
		return 1;
	}
	
	public boolean ehtiiKaasuttaa(double etaisyysKohteeseen, double nopeusKohteessa)
	{
		if (munauto.nopeus<nopeusKohteessa) return true;
			
		double nopeus2 = Math.pow(munauto.nopeus, 2.15);
		double kohdenop2 = (nopeusKohteessa * nopeusKohteessa);
		double minimi = nopeus2-kohdenop2;
				
		return etaisyysKohteeseen > minimi;  		
	}
	
	public double maxKaasu()
	{
		
		double angle = munauto.angle;
		if (angle<0) angle = -angle;
		
		if (munauto.pala.tyyppi == STRAIGHT  || munauto.heilahdusSuunta == STRAIGHT)
		{
			if (munauto.heilahdusSuunta==munauto.pala.tyyppi) return 1;
			if (munauto.heilahdusKiihtyvyys>0.28)
			{
				if (munauto.pala.edellinen.tyyppi==STRAIGHT)	munauto.pala.edellinen.edellinen.liianKovaa+=2;
				else 	munauto.pala.edellinen.liianKovaa+=2;
				return 0.9;
			}
			return 1;
		}

		if (munauto.pala.tyyppi == LEFT && munauto.angle>0 || munauto.pala.tyyppi == RIGHT && munauto.angle<0)
		{
			if (munauto.heilahdusKiihtyvyys>0.18 || munauto.heilahdus>5)
			{
				munauto.pala.edellinen.liianKovaa++;
				munauto.pala.liianKovaa+=2;
				return 0;
			}
			return 1;
		}
		
		if (angle<7-crashiKulma || angle > crashiKulma-7) return 0;
			
		double mutkaaJaljella = Ratapala.mutkaaJaljella(munauto.pala, munauto.paikka);
		
		double kiihtyvyys = munauto.heilahdusKiihtyvyys;
		if (kiihtyvyys>0) kiihtyvyys++; else kiihtyvyys = 1;
				
		//if (Math.pow(angle, 1.8) + kiihtyvyys*mutkaaJaljella/4 + munauto.heilahdus*mutkaaJaljella/2 > crashiKulma*15)
   	if (Math.pow(angle, 2) + kiihtyvyys*mutkaaJaljella/4 + munauto.heilahdus*mutkaaJaljella/2 > crashiKulma*27)		
		{
			munauto.pala.liianKovaa++;
			return 0;
		}
   	
   	if (munauto.vaihtaaKaistaa) return 0.5;
   	
		return 1;
	}
	
	public String vaihdaKaistaa()
	{
		if (vaihteetonKisa) return STRAIGHT;
		
		if (munauto.manooveri==START)
		{
			Ratapala p = munauto.pala;
			Ratapala vika = p.edellinen;
			while (!p.switchi  && p!=vika)
			{
				p.valittuAjoLinja = munauto.laneIndex;
				p = p.seuraava;
			}
			munauto.manooveri = NULL;
		}
		
		if (munauto.manooveri==NULL)
		{
			munauto.manooveri = munauto.pala.seuraavaVaihde.oletusManooveri[munauto.laneIndex];
			laskeValittuAjolinja(munauto.laneIndex, munauto.pala.seuraavaVaihde, munauto.manooveri);
			return munauto.manooveri;
		}
		return STRAIGHT;
	}
	
	public void laskeValittuAjolinja(int kaista, Ratapala vaihteesta, String manooveri)
	{
		Ratapala p = vaihteesta;
		if (manooveri==LEFT) kaista--;
		if (manooveri==RIGHT) kaista++;
		for (p = vaihteesta; p!=vaihteesta.seuraavaVaihde; p=p.seuraava) p.valittuAjoLinja=kaista;
		
		if (p.oletusManooveri[kaista]==LEFT) kaista--;
		if (p.oletusManooveri[kaista]==RIGHT) kaista++;
		for (; p!=vaihteesta.seuraavaVaihde; p=p.seuraava) p.valittuAjoLinja=kaista;
	}
		
	public void alustaVauhdinjako()
	{		
		final double huoletonRadius = 1000;

		Ratapala vika = rata[rata.length-1];
		Ratapala nyt = rata[0];
		
		while(true)
		{			
			if (nyt.oletusNopeus==null) nyt.oletusNopeus = new double[ajoLinjoja];
		
			for (int l=0; l<ajoLinjoja; l++)
			{
				if (nyt.radius[l]>=huoletonRadius || nyt.seuraava.tyyppi!=nyt.tyyppi) nyt.oletusNopeus[l] = 10000;
				else
				{
					double kokonaisJyrkkyys = Ratapala.kokonaisJyrkkyys(nyt, l);
					double mutkaaJaljella = Ratapala.mutkaaJaljella(nyt, l);
				  nyt.oletusNopeus[l] = nyt.radius[l] * kokonaisJyrkkyys/(rauhallisuus*Math.pow(mutkaaJaljella, 0.3));
					
				}
				
				if (nyt.edellinen.oletusNopeus!=null && nyt.oletusNopeus[l]*2<nyt.edellinen.oletusNopeus[l])
				{
					nyt.edellinen.oletusNopeus[l] = (nyt.oletusNopeus[l] +	nyt.edellinen.oletusNopeus[l])/2;
				}
			}
			
			

			if (nyt == vika) break;
			nyt = nyt.seuraava;
		}
	}
	
	public void palaVaihtuu()
	{
		System.out.print(munauto.pala.index+"  ");//+munauto.pala.maxKulma+" - "+munauto.pala.maxNopeus+"/"+munauto.pala.oletusNopeus[0]);
		valittuManooveri = NULL;
		
		if (turboKaytossa && munauto.angle<10  && munauto.angle>-10)
		{
			if ((munauto.pala.loppuSuora || munauto.pala.seuraava.seuraava.loppuSuora) && munauto.lap == vikaKierros)
	  	{
				main.send(main.new Turbo());
	  	  turboKaytossa = false;
	  	}
		}
		
		/*if (munauto.lap==0 && munauto.pala.index<20 && munauto.pala.maxKulma>58)
		{					
			for (int i = 0; i<rata.length; i++)
			{
				for (int l=0; l<ajoLinjoja; l++)
			  {	
					rata[i].oletusNopeus[l]*=0.9;
			  }
			}
		}*/
	}
	
	public void kierrosVaihtui(Auto auto)
	{
		auto.lap++;
		if (auto!=munauto) return;
		laskeOletusNopeudet();
		//omatCrashiKohdat.clear();
	}
		
	public void laskeOletusNopeudet()
	{
		for (int i = 0; i<rata.length; i++)
		{
			if (rata[i].oletusNopeus[rata[i].valittuAjoLinja]>20) continue;
		
			double kulma = rata[i].maxKulma;
			
			if (kulma>crashiKulma-2)
			{
				for (int l=0; l<ajoLinjoja; l++)
				{	
					rata[i].edellinen.oletusNopeus[l]*=0.95;
				}
				continue;
			}
			
			if (rata[i].maxNopeus/rata[i].oletusNopeus[rata[i].valittuAjoLinja]<0.9)
			{
			  if (munauto.lap==2 && kulma>crashiKulma-8)
			  {
				  for (int l=0; l<ajoLinjoja; l++)
				  {
				  	rata[i].oletusNopeus[l] = (rata[i].maxNopeus+rata[i].oletusNopeus[rata[i].valittuAjoLinja])/2;
				  	rata[i].edellinen.oletusNopeus[l] *= 0.95;
				  }
			  }
				continue;
			}
			
			if (omatCrashiKohdat.contains(rata[i].edellinen) || omatCrashiKohdat.contains(rata[i]) || omatCrashiKohdat.contains(rata[i].seuraava) || omatCrashiKohdat.contains(rata[i].seuraava.seuraava) || omatCrashiKohdat.contains(rata[i].seuraava.seuraava.seuraava)) continue;

			if (rata[i].seuraava.maxKulma - kulma<20 && kulma<crashiKulma-5 && rata[i].seuraava.maxKulma<crashiKulma-5 && rata[i].seuraava.liianKovaa<4  && rata[i].seuraava.seuraava.liianKovaa<4)
			{	 
				if (kulma<crashiKulma-10 && rata[i].seuraava.liianKovaa<2 && rata[i].seuraava.seuraava.liianKovaa<2)
				{	 
				  for (int l=0; l<ajoLinjoja; l++)
				  {	
					  rata[i].oletusNopeus[l]*=1.25;
				  }
				}
				else
				{
			    for (int l=0; l<ajoLinjoja; l++)
			    {	
				    rata[i].oletusNopeus[l]*=1.15;
			    }
				}			 
			}
			
			
		}
		
		for (int i = 0; i<rata.length; i++)
		{
			rata[i].maxKulma = 0;
			rata[i].maxNopeus = 0;
			rata[i].liianKovaa = 0;
		}
	}
}
