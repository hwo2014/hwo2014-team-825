package olli;


public class Ratapala
{
  boolean switchi = false;
  String tyyppi;
	
	int index;
	double[] radius;
	double[] length;
  
	double[] oletusNopeus;
	//double[] nopeudenKorjauskerroin;
	
	double maxKulma;
	double maxNopeus;
	int liianKovaa;

  Ratapala edellinen;
  Ratapala seuraava;
  
  Ratapala seuraavaVaihde;
  
  String[] oletusManooveri; 
  int valittuAjoLinja;
  
  boolean loppuSuora = false;
 	
   
  public static int etaisyys(Ratapala eka, Ratapala vika)
  {
  	int etaisyys = 0;
  	
  	Ratapala nyt = eka;
  	
  	while (nyt!=vika)
  	{
  		etaisyys+=nyt.length[nyt.valittuAjoLinja];
  		nyt = nyt.seuraava;
  	}
  	
  	return etaisyys;
  }
  
  public static int etaisyys(Ratapala eka, Ratapala vika, int linja)
  {
  	int etaisyys = 0;
  	
  	Ratapala nyt = eka;
  	
  	while (nyt!=vika)
  	{
  		etaisyys+=nyt.length[linja];
  		nyt = nyt.seuraava;
  	}
  	
  	return etaisyys;
  }
  
  public static double mutkaaJaljella(Ratapala pala, double paikka)
  {
    Ratapala nyt = pala.seuraava;
    
    double etaisyys = pala.length[nyt.valittuAjoLinja]-paikka;
  	
  	while (nyt.tyyppi == pala.tyyppi && nyt!=pala)
  	{
  		etaisyys+=nyt.length[nyt.valittuAjoLinja];
  		nyt = nyt.seuraava;
  	}
  	
  	return etaisyys;
  }
  
  public static double mutkaaJaljella(Ratapala pala, int kaista)
  {
    Ratapala nyt = pala.seuraava;
     
    double etaisyys = 0;
  	
  	while (nyt.tyyppi == pala.tyyppi && nyt!=pala)
  	{
  		etaisyys+=nyt.length[kaista];
  		nyt = nyt.seuraava;
  	}
  	
  	return etaisyys;
  }
  
  public static double kokonaisJyrkkyys(Ratapala pala, int kaista)
  {
  	Ratapala nyt = pala.seuraava;
    
    double etaisyys = pala.length[kaista];
    double jyrkkyys = pala.radius[kaista]*etaisyys;
  	  	
  	while (nyt.tyyppi == pala.tyyppi && nyt!=pala)
  	{
  		etaisyys+=nyt.length[kaista];
  		jyrkkyys+=nyt.radius[kaista]*nyt.length[kaista];
  		nyt = nyt.seuraava;
  	}
  	jyrkkyys/=etaisyys;
  	return jyrkkyys;
  }
  
 /* public static double tiukinKohta(Ratapala pala, int lane)
  {
  	Ratapala nyt = pala.seuraava;
    
    double tiukka = pala.radius[lane];
  	  	
  	while (nyt.tyyppi == pala.tyyppi && nyt!=pala)
  	{
  		if(nyt.radius[lane]<tiukka) tiukka = nyt.radius[lane];
  		nyt = nyt.seuraava;
  	}
  	return tiukka;
  }*/
}
