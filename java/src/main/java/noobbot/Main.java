package noobbot;

/*
testserver.helloworldopen.com 
hakkinen.helloworldopen.com (R2)
senna.helloworldopen.com (R1)
webber.helloworldopen.com (R3)

prost.helloworldopen.com
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import olli.AI_interface;
import olli.AI_kisakone;
import olli.AI_kori;
import olli.AI_simppeli;

import com.google.gson.Gson;

public class Main implements Runnable
{
	/*public static void o(Object o)
	{
		System.out.println(o);
	}*/

	/*public static void o(String auto, Object o)
	{
		if (auto.startsWith("Tustor") || auto.endsWith("0")) System.out.println(auto+": "+o);
	}*/

	public static void main(String... args) throws IOException
	{		
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		int autoja = 1;
		
		if (!botName.startsWith("aiolli")) autoja = 1;

		ArrayList<Thread> saikeet = new ArrayList<>();


		try
		{
			for (int a=0; a<autoja; a++)
			{
				Thread t = new Thread(new Main(host, port, botName+a, botKey, autoja));
				t.start();
				saikeet.add(t);
				Thread.sleep(500);
			}
			for (Thread t: saikeet) t.join();
		}
		catch (Exception t)
		{
			t.printStackTrace();
		} 
	}




	public final Gson gson = new Gson();
	private PrintWriter writer;
	String host;
	int port;
	String botName;
	String botKey;
	boolean create;
	int cars;


	public Main(final String host, final int port, String botName, String botKey, int cars)
	{
		this.host = host;
		this.port = port;
		this.botName = botName;
		this.botKey = botKey;
		this.cars = cars;
	}


	public void run()
	{
		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		Socket socket = null;

		try
		{
			socket = new Socket(host, port);
			final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));


			this.writer = writer;

			if (botName.startsWith("aiolli")) send(new JoinRace(botName, botKey, "suzuka", cars));
			else
			{
				System.out.println("Ajetaan serveri-buildilla");
				send(new Join(botName, botKey));   
			}

			AI_interface olli;
			olli = new AI_kisakone(this, reader);
			olli.drive();

			/*else
       {
       while((line = reader.readLine()) != null)
       {        	
       	final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
           if (msgFromServer.msgType.equals("carPositions")) {
               send(new Throttle(0.64));
           } else if (msgFromServer.msgType.equals("join")) {
               System.out.println("Joined");
           } else if (msgFromServer.msgType.equals("gameInit")) {
               System.out.println("Race init");
           } else if (msgFromServer.msgType.equals("gameEnd")) {
               System.out.println("Race end");
           } else if (msgFromServer.msgType.equals("gameStart")) {
               System.out.println("Race start");
           }
           else
           {
           	System.out.println(line);
           	send(new Ping());
           }
       }
       }*/
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
		finally
		{
			try
			{
				socket.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void send(final SendMsg msg)
	{
		writer.println(msg.toJson());
		writer.flush();
	}

	public abstract class SendMsg {
		public String toJson() {
			return new Gson().toJson(new MsgWrapper(this));
		}

		protected Object msgData() {
			return this;
		}

		protected abstract String msgType();
	}

	public class MsgWrapper {
		public final String msgType;
		public final Object data;

		MsgWrapper(final String msgType, final Object data) {
			this.msgType = msgType;
			this.data = data;
		}

		public MsgWrapper(final SendMsg sendMsg) {
			this(sendMsg.msgType(), sendMsg.msgData());
		}
	}

	class Join extends SendMsg {
		public final String name;
		public final String key;

		Join(final String name, final String key) {
			this.name = name;
			this.key = key;
		}

		@Override
		protected String msgType() {
			return "join";
		}
	}

	class BotId
	{  	
		public final String name;
		public final String key;

		BotId(final String name, final String key)
		{
			this.name = name;
			this.key = key;
		}
	}

	class CreateRace extends SendMsg
	{

		public final BotId botId;
		public final String trackName;
		public final int carCount;

		CreateRace(final String name, final String key, final String trackName, int cars)
		{
			this.botId = new BotId(name, key);
			this.trackName = trackName;
			this.carCount = cars;
		}

		@Override
		protected String msgType() {
			return "createRace";
		} 
	}

	class JoinRace extends SendMsg
	{

		public final BotId botId;
		public final String trackName;
		public final int carCount;
		public final String password = "password"; 

		JoinRace(final String name, final String key, final String trackName, int cars)
		{
			this.botId = new BotId(name, key);
			this.trackName = trackName;
			this.carCount = cars;
		}

		@Override
		protected String msgType()
		{
			return "joinRace";
		} 
	}

	public class Ping extends SendMsg
	{
		@Override
		protected String msgType() {
			return "ping";
		}
	}

	public class Turbo extends SendMsg {
		@Override
		protected String msgType() {
			return "turbo";
		}

		protected Object msgData()
		{
			return "data";
		}
	}

	public class Throttle extends SendMsg {
		private double value;

		public Throttle(double value) {
			this.value = value;
		}

		@Override
		protected Object msgData() {
			return value;
		}

		@Override
		protected String msgType() {
			return "throttle";
		}


	}

	public class SwitchLane extends SendMsg {
		private boolean right;

		public SwitchLane(boolean right) {
			this.right = right;
		}

		@Override
		protected Object msgData() {
			if (right) return "Right";
			return "Left";
		}

		@Override
		protected String msgType() {
			return "switchLane";
		}


	}
}